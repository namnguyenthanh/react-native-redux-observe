import React from 'react';
import {View, Text, StyleSheet, Image, SafeAreaView, Alert} from 'react-native';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import BaseAppBar from '../../components/appbar/app_bar';
import {RootStackParamList} from '../../components/navigation/config/Routes';
type Props = NativeStackScreenProps<RootStackParamList, 'DetailScreen'>;
export const DetailScreen = ({navigation, route}: Props) => {
  const props = route.params.item;

  return (
    <SafeAreaView style={styles.container}>
      <BaseAppBar
        title="Detail"
        onPress={() => {
          navigation.goBack();
        }}
        goBack={true}
      />
      <View style={styles.custom}>
        <Image style={styles.image} source={{uri: props.backdropPath ?? ''}} />
        <Text style={styles.title}>{props.originalTitle}</Text>
        <Text style={styles.overview}>{props.overview}</Text>
        <View style={styles.line} />
        <View style={styles.row}>
          <Text style={styles.overview}>Release date</Text>
          <Text style={styles.overview}>{props.releaseDate}</Text>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  custom: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    fontSize: 30,
  },
  image: {
    width: '100%',
    height: 200,
    resizeMode: 'cover',
  },
  title: {
    marginTop: 20,
    marginHorizontal: 16,
    fontSize: 30,
    fontWeight: '500',
  },
  overview: {
    fontSize: 16,
    marginTop: 20,
    marginHorizontal: 16,
  },
  line: {
    height: 1,
    marginTop: 20,
    backgroundColor: 'black',
  },
  row: {
    flexDirection: 'row',
  },
});

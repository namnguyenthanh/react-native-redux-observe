import {combineEpics} from 'redux-observable';
import {homeEpic} from '../home/home.epic';
export const rootEpic = combineEpics(homeEpic);

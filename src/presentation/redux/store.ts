import {combineReducers, configureStore} from '@reduxjs/toolkit';
import {createEpicMiddleware} from 'redux-observable';
import homeReducer from '../home/home.slice';
import {rootEpic} from './rootEpic';
const rootReducer = combineReducers({
  home: homeReducer,
});

const epicMiddleWare = createEpicMiddleware();
export const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware().concat(epicMiddleWare),
});
epicMiddleWare.run(rootEpic);
export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;

import React from 'react';
import {} from 'react-native';

import {useDispatch, useSelector, Selector} from 'react-redux';
import {RootState} from '../redux/store';
import {homeSlice, INIT_STATE} from './home.slice';
import {HomeState} from './type';

export const homeSelector: Selector<RootState, HomeState> = ({
  home = INIT_STATE,
}) => home;

const {
  actions: {fetch},
} = homeSlice;

export function useHomeModel() {
  const {data, fetching} = useSelector<RootState, HomeState>(homeSelector);
  const dispatch = useDispatch();

  const doFetch = React.useCallback(() => {
    dispatch(fetch());
  }, [dispatch]);

  return {data, fetching, doFetch};
}

import {Movie} from '../../domain/model/movie';

export type HomeState = {
  data: Movie[];
  fetching: boolean;
};
export type Section = {
  data: Movie[];
  page: number;
};
export type HomeProps = {};

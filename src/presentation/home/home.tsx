import React, {useEffect} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Movie} from '../../domain/model/movie';

import {useHomeModel} from './hook';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import BaseAppBar from '../../components/appbar/app_bar';
import {RootStackParamList} from '../../components/navigation/config/Routes';
type Props = NativeStackScreenProps<RootStackParamList, 'HomeScreen'>;
export const HomeScreen = ({navigation}: Props) => {
  const {data, doFetch} = useHomeModel();
  useEffect(() => {
    doFetch();
  }, [doFetch]);
  const renderItem = (item: Movie) => {
    const onPress = () => {
      navigation.navigate('DetailScreen', {item});
    };
    return (
      <TouchableOpacity onPress={onPress}>
        <View style={[styles.itemMovie, styles.shadowItem]}>
          <Image style={styles.image} source={{uri: item.posterPath}} />
          <View style={styles.column}>
            <Text style={styles.title}>{item.title}</Text>
            <Text style={styles.overview} numberOfLines={5}>
              {item.overview}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <BaseAppBar title="HomePage" goBack={false} />
      <FlatList
        style={styles.flatList}
        data={data}
        renderItem={value => renderItem(value.item)}
        keyExtractor={item => item.title ?? ''}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  title: {
    fontSize: 20,
    flex: 1,
  },
  overview: {
    flex: 1,
  },
  flatList: {
    paddingTop: 16,
    marginHorizontal: 16,
  },
  itemMovie: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginVertical: 8,
    borderRadius: 8,
    borderColor: 'black',
    borderWidth: 0.5,
  },
  shadowItem: {
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 7,
  },
  image: {
    height: 150,
    width: 100,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
    resizeMode: 'cover',
  },
  column: {
    flexDirection: 'column',
    flex: 1,
    paddingHorizontal: 8,
    paddingVertical: 8,
  },
});

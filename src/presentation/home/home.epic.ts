import {Epic, combineEpics} from 'redux-observable';
import {filter, map, catchError, mergeMap} from 'rxjs/operators';
import {of, concat} from 'rxjs';
import {homeSlice} from './home.slice';
import {container} from 'tsyringe';
import {AppDependencies} from '../../data/di/type';
import {HomeRepository} from '../../domain/repository/home/HomeRepository';

const {
  actions: {fetch, fetchSuccess, fetchFailed},
} = homeSlice;
const repo = container.resolve<HomeRepository>(AppDependencies.HomeRepository);
const fetchMovieEpic$: Epic = action$ =>
  action$.pipe(
    filter(fetch.match),
    mergeMap(action => {
      return concat(
        repo.getListMovie(action.payload).pipe(
          map(profile => fetchSuccess(profile.data ?? [])),
          catchError(err => {
            console.log(err);
            return of(fetchFailed());
          }),
        ),
      );
    }),
  );

export const homeEpic = combineEpics(fetchMovieEpic$);

import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {Movie} from '../../domain/model/movie';
import {HomeState} from './type';

export const INIT_STATE: HomeState = {
  data: [],
  fetching: false,
};
export const homeSlice = createSlice({
  name: 'home',
  initialState: INIT_STATE,
  reducers: {
    fetch: state => Object.assign(state, {fetching: true}),
    fetchSuccess: (state, {payload}: PayloadAction<Movie[]>) =>
      Object.assign(state, {data: payload, fetching: false}),
    fetchFailed: state => Object.assign(state, {fetching: false}),
  },
});
export const homeAction = homeSlice.actions;
//Reducer
const homeReducer = homeSlice.reducer;
export default homeReducer;

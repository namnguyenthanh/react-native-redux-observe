import {Observable} from 'rxjs';
import {Exception} from '../../../data/exception/Exception';
import {Results} from '../../../data/result/Result';
import {Movie} from '../../../domain/model/movie';

export interface HomeRepository {
  getListMovie(page?: number): Observable<Results<Exception, Movie[]>>;
}

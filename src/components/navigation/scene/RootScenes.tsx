import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import navigationConfigs from '../config/Options';
import {RootStackParamList} from '../config/Routes';
import {HomeScreen} from '../../../presentation/home/home';
import {DetailScreen} from '../../../presentation/detail/detail';

const RootStack = createNativeStackNavigator<RootStackParamList>();

const RootScenes = () => {
  return (
    <RootStack.Navigator screenOptions={navigationConfigs}>
      <RootStack.Screen name="HomeScreen" component={HomeScreen} />
      <RootStack.Screen name="DetailScreen" component={DetailScreen} />
    </RootStack.Navigator>
  );
};

export default RootScenes;

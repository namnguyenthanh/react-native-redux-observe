/* eslint-disable @typescript-eslint/no-unused-vars */

import {
  CardStyleInterpolators,
  StackNavigationOptions,
} from '@react-navigation/stack';
import {Platform} from 'react-native';
import {Colors} from '../../../config/resources/Colors';

import transition from './Transition';

const navigationConfigs = {
  cardStyle: {
    backgroundColor: Colors.WHITE,
    // paddingBottom: Metrics.safeBottomPadding,
  },
  headerShown: false,
  gestureEnabled: true,
  // gestureDirection: 'default',
  cardShadowEnabled: true,
  keyboardHandlingEnabled: Platform.OS === 'ios',
  cardOverlayEnabled: true,
  cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
  transitionSpec: {
    open: transition,
    close: transition,
  },
};

export default navigationConfigs;

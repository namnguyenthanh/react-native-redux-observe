import {Movie} from '../../../domain/model/movie';

export type RootStackParamList = {
  HomeScreen: {name: string}; // name of screen and params for it
  DetailScreen: {item: Movie};
};

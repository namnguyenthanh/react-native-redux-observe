import React from 'react';
import {Image, SafeAreaView, StyleSheet, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Text} from 'react-native-paper';
import {BACK} from '../../../assets/images';
export interface AppBarProps {
  title: string;
  goBack: boolean;
  onPress?: () => void;
}
const BaseAppBar: React.FC<AppBarProps> = props => {
  return (
    <SafeAreaView>
      <View style={[styles.card, styles.shadowProp]}>
        <Text style={styles.text}>{props.title}</Text>
        <TouchableOpacity onPress={props.onPress} />
        {props.goBack ? <Image style={styles.icon} source={BACK} /> : <View />}
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },

  text: {
    fontSize: 20,
    textAlign: 'center',
  },
  card: {
    backgroundColor: 'white',
    height: 64,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  shadowProp: {
    shadowColor: '#171717',
    shadowOffset: {width: -2, height: 4},
    shadowOpacity: 0.2,
    shadowRadius: 3,
  },
  icon: {
    marginLeft: 16,
    position: 'absolute',
    left: 0,
  },
});
export default BaseAppBar;

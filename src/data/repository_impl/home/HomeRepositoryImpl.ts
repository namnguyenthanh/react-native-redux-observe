import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {autoInjectable, inject} from 'tsyringe';
import {Exception} from '../../exception/Exception';

import {toDomain} from '../../response/Movie/MovieResponse';
import {Results} from '../../result/Result';
import {HomeService} from '../../services/home/HomeService';
import {Movie} from '../../../domain/model/movie';
import {HomeRepository} from '../../../domain/repository/home/HomeRepository';

@autoInjectable()
export class HomeRepositoryImpl implements HomeRepository {
  constructor(
    @inject('HomeService')
    private readonly service: HomeService,
  ) {}
  getListMovie(page?: number): Observable<Results<Exception, Movie[]>> {
    return this.service.getListMovie(page).pipe(
      map((value): Results<Exception, Movie[]> => {
        return Results.data(value.results.map(toDomain));
      }),
      catchError(error => {
        console.log(error);
        return of(Results.error(error));
      }),
    );
  }
}

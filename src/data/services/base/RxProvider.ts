import axios, {
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse,
  AxiosError,
} from 'axios';
import * as AxiosLogger from 'axios-logger';
import {jsonBeautify} from './LoggerRespose';
import {RemoteException} from '../../exception/Exception';
import {Observable, Observer} from 'rxjs';

export interface RxRemoteProvider {
  /**
   * @summary perform @POST request with config
   * @param url
   * @param data
   *
   * @returns Either Axios response with generic data: T or @RemoteException if failed
   */
  post<T>(url: string, data: any): Observable<AxiosResponse<T>>;

  /**
   * @summary perform @GET request with config
   * @param url
   *
   * @returns Either Axios response with generic data: T or @RemoteException if failed
   */
  get<T>(url: string, data?: any): Observable<AxiosResponse<T>>;

  /**
   * @summary perform @PUT request with config
   * @param url
   * @param data
   *
   * @returns Either Axios response with generic data: T or @RemoteException if failed
   */
  put<T>(url: string, data: any): Observable<AxiosResponse<T>>;

  /**
   * @summary perform @DELETE request with config
   * @param url
   *
   * @returns Either Axios response with generic data: T or @RemoteException if failed
   */
  delete<T>(url: string): Observable<AxiosResponse<T>>;
}

export class RxServices implements RxRemoteProvider {
  private readonly axiosInstance: AxiosInstance;

  private token?: string;

  constructor(config: AxiosRequestConfig) {
    this.axiosInstance = axios.create(config);
    this.axiosInstance.interceptors.request.use(
      AxiosLogger.requestLogger,
      error => {
        console.log(error);
      },
    );
    this.axiosInstance.interceptors.response.use(
      response => {
        return AxiosLogger.responseLogger(response, {
          prefixText: response.status.toString(),
          logger: () => jsonBeautify(response),
        });
      },
      error => {
        console.log(error);
      },
    );
  }

  request<T>(requestConfig: AxiosRequestConfig): Observable<AxiosResponse<T>> {
    return new Observable((observer: Observer<AxiosResponse<T>>) => {
      try {
        this.axiosInstance.request(requestConfig).then(data => {
          observer.next(data);
          observer.complete();
        });
      } catch (error) {
        observer.error(new RemoteException<AxiosError>(error as any));
      }
    });
  }

  post<T>(url: string, data: any): Observable<AxiosResponse<T>> {
    return this.request({
      method: 'POST',
      data,
      url,
    });
  }
  get<T>(url: string, data?: any): Observable<AxiosResponse<T>> {
    return this.request({
      method: 'GET',
      url,
      data,
    });
  }
  put<T>(url: string, data: any): Observable<AxiosResponse<T>> {
    return this.request({
      method: 'PUT',
      data,
      url,
    });
  }
  delete<T>(url: string): Observable<AxiosResponse<T>> {
    return this.request({
      method: 'DELETE',
      url,
    });
  }
}

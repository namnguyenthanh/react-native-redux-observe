import {AxiosResponse} from 'axios';

// This is to tell JSON.stringify to treat regular expressions as strings else it will serialize them as empty objects.
function replacer(key: any, value: any) {
  if (value instanceof RegExp) {
    return value.toString();
  } else {
    return value;
  }
}

export const jsonBeautify = function (data: AxiosResponse<any, any>) {
  let json = data.data;
  if (typeof json !== 'string') {
    json = JSON.stringify(json, replacer, 2);
  }
  console.log(
    `[${data.status}] ` +
      '[Response] ' +
      `${data.config.method} `.toUpperCase() +
      data.request.responseURL,
  );
  console.log(json);
};

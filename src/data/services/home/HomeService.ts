import {map, Observable} from 'rxjs';
import {injectable, inject} from 'tsyringe';

import {ApiConstants} from '../../../ultis/constant/ApiConstants';
import {AppDependencies} from '../../di/type';
import {DataMovieResponse} from '../../response/Movie/MovieResponse';
import {RxRemoteProvider} from '../base/RxProvider';

@injectable()
export class HomeService {
  constructor(
    @inject(AppDependencies.ApiProvider)
    private readonly provider: RxRemoteProvider,
  ) {}

  getListMovie(page?: number): Observable<DataMovieResponse> {
    const pageIndex = page ?? 1;

    return this.provider
      .get<DataMovieResponse>(
        `${ApiConstants.GET_MOVIE}?api_key=${ApiConstants.API_KEY}&page=${pageIndex}`,
      )
      .pipe(map(response => response.data));
  }
}

import {Movie} from '../../../domain/model/movie';
import {ApiConstants} from '../../../ultis/constant/ApiConstants';

export interface DataMovieResponse {
  page: number;
  results: MovieResponse[];
  total_pages: number;
  total_results: number;
}

export interface MovieResponse {
  adult: boolean;
  backdrop_path: string;
  genre_ids: number[];
  id: number;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  release_date: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
}

export const toDomain = (data: MovieResponse) => {
  return {
    posterPath: `${ApiConstants.BASE_IMG_200}${data.poster_path}`,
    adult: data.adult,
    overview: data.overview,
    releaseDate: data.release_date,
    genreIds: data.genre_ids,
    id: data.id,
    originalTitle: data.original_title,
    originalLanguage: data.original_language,
    title: data.title,
    backdropPath: `${ApiConstants.BASE_IMG_400}${data.backdrop_path}`,
    popularity: data.popularity,
    voteCount: data.vote_count,
    video: data.video,
    voteAverage: data.vote_average,
  } as Movie;
};

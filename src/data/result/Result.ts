interface Props<Error, Data> {
  error?: Error | null;
  data?: Data | null;
}
export class Results<Error, Data> {
  error?: Error | null;
  data?: Data | null;
  get isData() {
    return this.data != null;
  }
  get isError() {
    return this.error == null;
  }
  constructor(props: Props<Error, Data>) {
    this.error = props.error;
    this.data = props.data;
  }
  static error = (error: any | null) => {
    return new Results<any, any>({
      error: error,
      data: null,
    });
  };
  static data = (data: any | null) => {
    return new Results<any, any>({
      error: null,
      data: data,
    });
  };
}

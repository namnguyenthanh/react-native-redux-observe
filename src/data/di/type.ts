export enum AppDependencies {
  StoreContainer = 'StoreContainer',
  ApiProvider = 'ApiProvider',
  HomeService = 'HomeService',
  HomeRepository = 'HomeRepository',
}

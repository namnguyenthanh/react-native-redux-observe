import {container} from 'tsyringe';
import {BuildConfig} from '../../config/base/BuildConfig';
import {AppDependencies} from './type';
import 'reflect-metadata';
import {RxServices} from '../services/base/RxProvider';
import {HomeService} from '../services/home/HomeService';
import {HomeRepository} from '../../domain/repository/home/HomeRepository';
import {HomeRepositoryImpl} from '../repository_impl/home/HomeRepositoryImpl';

export function registerDependencies() {
  container.register(AppDependencies.ApiProvider, {
    useValue: new RxServices({
      baseURL: BuildConfig.ApiUrl,
    }),
  });
  container.register(AppDependencies.HomeService, {
    useClass: HomeService,
  });

  container.register<HomeRepository>(AppDependencies.HomeRepository, {
    useClass: HomeRepositoryImpl,
  });
}

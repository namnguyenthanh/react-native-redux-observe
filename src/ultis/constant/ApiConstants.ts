export enum ApiConstants {
  GET_MOVIE = '/3/movie/popular',
  API_KEY = '73a40ff16270bb9ce947f98a47df9689',
  BASE_IMG_200 = 'https://image.tmdb.org/t/p/w200',
  BASE_IMG_400 = 'https://image.tmdb.org/t/p/w500',
}

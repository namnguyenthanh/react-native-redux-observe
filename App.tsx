import React from 'react';
import {Provider} from 'react-redux';
import {registerDependencies} from './src/data/di/DataModules';
import {NavigationContainer} from '@react-navigation/native';
import {store} from './src/presentation/redux/store';
import {navigationRef} from './src/components/navigation/NavigationService';
import RootScenes from './src/components/navigation/scene/RootScenes';
registerDependencies();
const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer ref={navigationRef}>
        <RootScenes />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
